//
//  CameraTableViewCell.swift
//  Track360
//
//  Created by Conjure on 16/04/2018.
//  Copyright © 2018 Conjure. All rights reserved.
//

import UIKit

class CameraTableViewCell: UITableViewCell {
    
    // MARK: - Initialization
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: .default, reuseIdentifier: "CameraTableViewCell")
        
        // settings
        selectionStyle = .none
        contentView.backgroundColor = .clear
        backgroundColor = .clear
        
        // text
        textLabel?.numberOfLines = 0
        textLabel?.textColor = .white
        
        // accessory
        let image = UIImage(named: "camera_connect_tick")
        let accessoryTickView = UIImageView(image: image)
        accessoryView = accessoryTickView
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    // MARK: - Layout
    override func layoutSubviews() {
        super.layoutSubviews()
        
        // accessory view
        if accessoryView != nil {
            let posX = contentView.frame.width - separatorInset.right + accessoryView!.frame.width * 0.5
            var frame = accessoryView!.frame
            frame.origin.x = posX
            accessoryView!.frame = frame
        }
    }
    
    // MARK: - Lifecycle
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // tick
        accessoryView?.isHidden = !selected
    }
}

// MARK: - Reusable
extension CameraTableViewCell: Reusable {}
