//
//  GalleryTableViewCell.swift
//  Track360
//
//  Created by Dinesh Vijaykumar on 17/04/2018.
//  Copyright © 2018 Conjure. All rights reserved.
//

import UIKit
import Fly360SDK

class GalleryTableViewCell: UITableViewCell {
    
    @IBOutlet weak var thumbnailImageView: UIImageView!
    @IBOutlet weak var dateLabel: UILabel!
    
    fileprivate var media: FLYDeviceMedia!
    var dateFormatter = DateFormatter()
    var buttonCallback: ((FLYDeviceMedia) -> ())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        
        self.dateFormatter.dateFormat = "dd/MM/yy"
    }
    
    func updateCell(media:FLYDeviceMedia) {
        self.media = media
        let date = dateFormatter.string(from: media.modificationDate)
        dateLabel.text = date
    }
    
    
    @IBAction func downloadMedia(_ sender: Any) {
        self.buttonCallback?(media)
    }
    
}
