//
//  TimerCollectionViewCell.swift
//  Track360
//
//  Created by Dinesh Vijaykumar on 13/04/2018.
//  Copyright © 2018 Conjure. All rights reserved.
//

import UIKit

class TimerCollectionViewCell: UICollectionViewCell {
    @IBOutlet var textLabel:UILabel!
    
    func setupCell(row:Int) {
        let (min, sec) = secondsToMinutesSecond(second: row)
        textLabel.text = "\(String(format: "%02d", min)):\(String(format: "%02d", sec))"
    }
    
    fileprivate func secondsToMinutesSecond(second:Int) -> (Int, Int) {
        return (((second % 3600) / 60), (second % 3600) % 60)
    }
}
