//
//  FilterView.swift
//  TFLHealth
//
//  Created by Conjure on 23/10/2017.
//  Copyright © 2017 Conjure. All rights reserved.
//

import UIKit

protocol FilterViewDataSource: NSObjectProtocol {
    func numberOfItems(filterView: FilterView) -> Int
    func itemAt(index: Int, filterView: FilterView) -> FilterViewItem
}

protocol FilterViewItem: NSObjectProtocol {
    var contentView: UIView { get }
    var isSelected: Bool { get set }
}

@IBDesignable
class FilterView: XibBaseView {
    
    // MARK: - Outlets
    @IBOutlet private(set) weak var scrollView: UIScrollView!
    
    // MARK: - Properties
    weak var dataSource: FilterViewDataSource?
    var selectionCallback: ((_ index: Int) ->())!
    var itemSpacing: CGFloat = 10
    var margin: CGFloat = 16
    
    private var items = [FilterViewItem]()
    private(set) var selectedIndex = 0
    
    // MARK: - Initialization
    override func setup() {
        super.setup()
    }
    
    // MARK: - Lifecycle
    func reloadData() {
        // clean
        for view in scrollView.subviews {
            view.removeFromSuperview()
        }
        items.removeAll()
        
        // parameters
        guard let itemCount = dataSource?.numberOfItems(filterView: self) else {
            return
        }
        var contentWidth = margin
        var contentEndX: CGFloat = margin
        
        // items
        for i in 0..<itemCount {
            guard let itemView = dataSource?.itemAt(index: i, filterView: self) else {
                continue
            }

            let itemSize = itemView.contentView.frame.size
            let centerY = (bounds.height - itemSize.height) * 0.5 + itemSize.height * 0.5
            let centerX = contentEndX + itemSize.width * 0.5
            itemView.contentView.center = CGPoint(x: centerX, y: centerY)
            
            contentEndX = itemView.contentView.frame.origin.x + itemView.contentView.frame.width + itemSpacing
            contentWidth += itemView.contentView.frame.width + itemSpacing
            
            scrollView.addSubview(itemView.contentView)
            items.append(itemView)
        }
        contentWidth -= itemSpacing
        contentWidth += margin
        
        // content size
        scrollView.contentSize = CGSize(width: contentWidth, height: bounds.height)
        if contentWidth > bounds.width {
            scrollView.isScrollEnabled = true
        }
        
        // first selection
        if selectedIndex == 0 && items.count > 0 {
            let item = items.first!
            item.isSelected = true
        }
    }
    
    func selectItem(_ selected: Bool, index: Int) {
        // assertion
        if index == selectedIndex || index >= items.count {
            return
        }
        
        // select
        let itemPrevious = items[selectedIndex]
        itemPrevious.isSelected = !selected
        let itemNext = items[index]
        itemNext.isSelected = selected
        selectedIndex = index
    }
    
    // MARK: - IB Actions
    @IBAction func onTapGesture(_ sender: UITapGestureRecognizer) {
        let point = sender.location(in: scrollView)
        
        // item index
        var index = Int.max
        for i in 0..<scrollView.subviews.count {
            let item = scrollView.subviews[i]
            if point.x > item.frame.origin.x && point.x < (item.frame.origin.x + item.frame.width) {
                index = i
                break
            }
        }
        
        // selected
        if index != Int.max {
            let item = items[index]
            selectItem(!item.isSelected, index: index)
            selectedIndex = index
        }
        
        // callback
        if index != Int.max {
            selectionCallback?(index)
        }
    }
}
