//
//  TabFilterViewItem.swift
//  BBTours
//
//  Created by Conjure on 25/10/2017.
//  Copyright © 2017 Conjure. All rights reserved.
//

import UIKit

class TabFilterViewItem: XibBaseView {
    
    // MARK: - Outlets
    @IBOutlet weak var normalLabel: UILabel!
    @IBOutlet weak var selectedLabel: UILabel!
    @IBOutlet weak var bottomView: UIView!
    
    // MARK: - Properties
    fileprivate var _isSelected = false
    private var colorSelected: UIColor!
    
    // MARK: - Initialization
    override func setup() {
        super.setup()
        
        // state
        updateState()
    }
    
    // MARK: - Lifecycle
    func updateState() {
        // text
        animateState(_isSelected)
        
        // bottom view
        bottomView.backgroundColor = _isSelected ? colorSelected : UIColor.darkGray
    }
    
    func animateState(_ selected: Bool) {
        UIView.animate(withDuration: 0.25) {
            self.normalLabel.alpha = selected ? 0 : 1
            self.selectedLabel.alpha = selected ? 1 : 0
            self.bottomView.backgroundColor = selected ? self.colorSelected : UIColor.darkGray
        }
    }
}

// MARK: - FilterViewItem
extension TabFilterViewItem: FilterViewItem {
    
    var contentView: UIView {
        return self
    }
    
    var isSelected: Bool {
        get {
            return _isSelected
        }
        set {
            _isSelected = newValue
            updateState()
        }
    }
}

// MARK: - Title
extension TabFilterViewItem {
    
    convenience init(tabTitle: String) {
        self.init()
        
        // color
        colorSelected = .red
        
        // normal text
        let normalText = NSMutableAttributedString(string: tabTitle)
        normalText.setColor(color: .white, forText: tabTitle)
        normalLabel.attributedText = normalText
        
        // selected text
        let selectedText = NSMutableAttributedString(string: tabTitle)
        selectedText.setColor(color: .white, forText: tabTitle)
        selectedLabel.attributedText = selectedText
        selectedLabel.alpha = 0
    }
}
