//
//  MediaCell.swift
//  Track360
//
//  Created by Dinesh Vijaykumar on 08/03/2018.
//  Copyright © 2018 Conjure. All rights reserved.
//

import UIKit
import FlyDeviceSDK

class MediaCell: UITableViewCell {
    
    // MARK: - Outlets
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var previewImageView: UIImageView!
    
    // MARK: - Properties
    var viewModel: MediaCellViewModel!
    var media: FLYDeviceMedia! {
        didSet {
            viewModel = MediaCellViewModel(media: media)
        }
    }
    
    // MARK: - Initialization
    override func awakeFromNib() {
        super.awakeFromNib()
        
        selectionStyle = .none
    }

    // MARK: - Lifecycle
    func updateCell() {
        self.titleLabel.text = viewModel.title
        self.dateLabel.text = viewModel.date
        self.timeLabel.text = viewModel.duration
    }

}
