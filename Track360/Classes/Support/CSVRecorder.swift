//
//  CSVRecorder.swift
//  Track360
//
//  Created by Conjure on 12/03/2018.
//  Copyright © 2018 Conjure. All rights reserved.
//

import Foundation
import CSV

public class CSVRecorder {

    // MARK: - Properties
    private var csv: CSVWriter?
    private var dateFormatter: DateFormatter!
    
    // MARK: - Initialization
    init() {
        // csv writer
        let stream = OutputStream(toMemory: ())
        csv = try! CSVWriter(stream: stream)
        
        // formatter
        dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .medium
        dateFormatter.timeStyle = .medium
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
    }
    
    // MARK: - Lifecycle
    func startRecordingWithHeader(_ header: [String]) {
        // csv
        let stream = OutputStream(toMemory: ())
        csv = try! CSVWriter(stream: stream)
        
        // header
        try! csv?.write(row: header)
    }
    
    func recordRow(_ row: [String]) {
        // new row
        csv?.beginNewRow()
        
        // write
        for str in row {
            do {
                try csv?.write(field: str)
            } catch let error {
                print(error)
            }
        }
    }
    
    @discardableResult func stopRecording() -> URL {
        // close
        csv?.stream.close()
        
        // get a String
        let csvData = csv?.stream.property(forKey: .dataWrittenToMemoryStreamKey) as! NSData
        let csvString = String(data: Data(referencing: csvData), encoding: .utf8)!
        
        // save
        let now = Date()
        let dateStr = dateFormatter.string(from: now).replacingOccurrences(of: " ", with: "").replacingOccurrences(of: ",", with: "")
        let filename = dateStr + ".csv"
        let cacheDir = NSSearchPathForDirectoriesInDomains(.cachesDirectory, .userDomainMask, true).first!
        let filepath = cacheDir + "/" + filename
        let url = URL(fileURLWithPath: filepath)
        do {
            try csvString.write(to: url, atomically: true, encoding: .utf8)
        } catch let error {
            print(error)
        }
        
        print(url)
        return url
    }
    
}
