//
//  ScreenViewController.swift
//  Track360
//
//  Created by Conjure on 13/04/2018.
//  Copyright © 2018 Conjure. All rights reserved.
//

import UIKit

class ScreenViewController: UIViewController {

    // MARK: - Outlets
    @IBOutlet weak var backgroundView: UIImageView!
    
    // MARK: - Initialization
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // MARK: - Lifecycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // status bar
        UIApplication.shared.statusBarStyle = .lightContent
        
        // navigation
        navigationController?.setNavigationBarHidden(false, animated: true)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.tintColor = UIColor.white
        navigationController?.navigationBar.titleTextAttributes = [
            .font: UIFont(name: "Roboto-Medium", size: 18)!,
            .foregroundColor: UIColor.white
        ]
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
}
