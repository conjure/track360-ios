//
//  PreviewViewController.swift
//  Track360
//
//  Created by Dinesh Vijaykumar on 16/04/2018.
//  Copyright © 2018 Conjure. All rights reserved.
//

import UIKit
import Fly360SDK

class PreviewViewController: UIViewController {

    @IBOutlet weak var thumbnailImageView: UIImageView!
    @IBOutlet weak var uploadingLabel: UILabel!
    @IBOutlet weak var uploadingWidthConstraint: NSLayoutConstraint!
    
    var media: FLYDeviceMedia!
    var connectedDevice: FLYConnectedDevice?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Preview"
        
        let alert = UIAlertController(title: "Video Uploading", message: "", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default) { (action) in
            DispatchQueue.main.async {
                self.uploadingLabel.text = "UPLOADING"
                UIView.animate(withDuration: 3.0, animations: {
                    let screenWidth = self.view.frame.size.width
                    self.uploadingWidthConstraint.constant = screenWidth
                    self.view.layoutIfNeeded()
                })
            }
        }
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func saveMedia(_ sender: Any) {
        let arr = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        if let documentsUrl = arr.first {
            let fileName = documentsUrl.appendingPathComponent(media.downloadURL.lastPathComponent)
            
            FLY360Helper.sharedInstance().downloadVideo(self.connectedDevice!, video: media, toLocation: fileName, withCompletionHandler: { (error) in
                if let error = error {
                    print("Error downloading video: \(error)")
                } else {
                    print("Downloaded \(fileName.lastPathComponent) - \(Date())")
                    // update
                    let session = SessionHelper.shared.getAllSessions()?.filter({ $0.recordingName! == self.media.name }).first
                    if session != nil {
                        SessionHelper.shared.update {
                            session!.isLocalCopy = true
                        }
                    }
                }
            })
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let playbackViewController = segue.destination as? PlaybackOptionsViewController {
            playbackViewController.video = self.media
        }
    }
}
