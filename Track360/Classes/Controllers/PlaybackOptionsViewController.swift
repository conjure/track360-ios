//
//  PlaybackOptionsViewController.swift
//  Track360
//
//  Created by Dinesh Vijaykumar on 17/04/2018.
//  Copyright © 2018 Conjure. All rights reserved.
//

import UIKit
import Fly360SDK

class PlaybackOptionsViewController: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var containerView: UIView!
    
    // MARK: - Properties
    var video: FLYDeviceMedia!
    
    // MARK: - Initialization
    override func viewDidLoad() {
        super.viewDidLoad()

        containerView.layer.borderColor = UIColor(hexString: "979797").cgColor
        containerView.layer.borderWidth = 1.0
    }

    // MARK: - IB Actions
    @IBAction func playOnPhone(_ sender: Any) {
        // stream
        let playerVC = FLY360SDK.sharedInstance().getPlayerFor(video)
        self.present(playerVC, animated: true, completion: nil)
    }
    
    @IBAction func playOnHeadset(_ sender: Any) {
    }
    
    // MARK: - Lifecycle
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        
        self.dismiss(animated: true, completion: nil)
    }

}
