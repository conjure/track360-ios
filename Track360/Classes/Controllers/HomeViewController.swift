//
//  HomeViewController.swift
//  Track360
//
//  Created by Conjure on 13/04/2018.
//  Copyright © 2018 Conjure. All rights reserved.
//

import UIKit

class HomeViewController: ScreenViewController {

    // MARK: - Outlets
    @IBOutlet weak var recordButton: UIButton!
    @IBOutlet weak var galleryButton: UIButton!
    
    // MARK: - Initialization
    override func viewDidLoad() {
        super.viewDidLoad()

        // title
        if CameraHelper.shared.discoveredDevice != nil {
            title = CameraHelper.shared.discoveredDevice!.name + " CONNECTED"
        }
        
        // adjust buttons
        let topInset: CGFloat = -5
        if UIScreen.main.bounds.width == 375 {
            recordButton.titleEdgeInsets = UIEdgeInsetsMake(topInset, recordButton.frame.width * 0.058, 0, 0)
            galleryButton.titleEdgeInsets = UIEdgeInsetsMake(topInset, galleryButton.frame.width * 0.058, 0, 0)
        } else if UIScreen.main.bounds.width == 414 {
            recordButton.titleEdgeInsets = UIEdgeInsetsMake(topInset, recordButton.frame.width * 0.067, 0, 0)
            galleryButton.titleEdgeInsets = UIEdgeInsetsMake(topInset, galleryButton.frame.width * 0.067, 0, 0)
        } else if UIScreen.main.bounds.width == 320 {
            recordButton.titleEdgeInsets = UIEdgeInsetsMake(topInset, recordButton.frame.width * 0.048, 0, 0)
            galleryButton.titleEdgeInsets = UIEdgeInsetsMake(topInset, galleryButton.frame.width * 0.048, 0, 0)
        }
    }

    // MARK: - Lifecycle
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let recordingViewController = segue.destination as? RecordingViewController {
            recordingViewController.discoveredDevice = CameraHelper.shared.discoveredDevice
        } else if let galleryViewController = segue.destination as? GalleryViewController {
            galleryViewController.discoveredDevice = CameraHelper.shared.discoveredDevice
        }
    }
}
