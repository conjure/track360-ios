//
//  RecordingViewController.swift
//  Track360
//
//  Created by Conjure on 13/04/2018.
//  Copyright © 2018 Conjure. All rights reserved.
//

import UIKit
import Fly360SDK
import AutoAPI
import HMKit

class RecordingViewController: ScreenViewController {

    @IBOutlet weak var growingView: UIView!
    @IBOutlet weak var separatorView: UIView!
    @IBOutlet weak var viewWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var secLabel: UILabel!
    @IBOutlet weak var msLabel: UILabel!
    @IBOutlet weak var minLabel: UILabel!
    @IBOutlet weak var recordingButton: UIButton!
    @IBOutlet weak var recordingLabel: UILabel!
    
    var activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.white)
    
    private var csvRecorder: CSVRecorder!
    private var csvTimer: Timer?
    private var csvStartTime: DispatchTime?
    
    var discoveredDevice:FLYDiscoveredDevice!
    var connectedDevice: FLYConnectedDevice?
    
    var isRecording: Bool = false
    let MAX_ITEMS = 601
    
    var labelTimer:Timer?
    var timer:Timer?
    var startTime = TimeInterval()
    var point:CGPoint = CGPoint(x: 0, y: 0)

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Recording"
        viewWidthConstraint.constant = 0
        
        recordingButton.isEnabled = false
        
        activityIndicator.hidesWhenStopped = true
        activityIndicator.center = view.center
        view.addSubview(activityIndicator)
        
        // csv recorder
        csvRecorder = CSVRecorder()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //TODO: Show missing connection popup
        if discoveredDevice != nil {
            activityIndicator.startAnimating()
            
            FLY360Helper.sharedInstance().connect(to: discoveredDevice!) { (connectedDevice, error) in
                self.activityIndicator.stopAnimating()
                if let error = error {
                    self.showAlert("Connection Error", message: error.localizedDescription)
                } else {
                    if let connected = connectedDevice {
                        self.connectedDevice = connected
                        
                        self.recordingButton.isEnabled = true
                    }
                }
            }
        }
    }
    
    @IBAction func recordButtonPressed(_ sender: UIButton) {
        // setup
        timer?.invalidate()
        activityIndicator.startAnimating()
        
        // record
        if !isRecording {
            CarHelper.shared.connect { (car, errorReason) in
                if car != nil {
                    print("Connected to car: \(car!.name!)")
                    
                    // start video recording
                    FLY360Helper.sharedInstance().startRecording(self.connectedDevice!, withCompletionHandler: { (error) in
                        if error != nil {
                            self.showAlert("Camera error", message: error!.localizedDescription)
                            
                            // stop timer
                            self.csvTimer?.invalidate()
                            self.csvTimer = nil
                            self.activityIndicator.stopAnimating()
                            return
                        }
                        
                        self.recordingLabel.text = "STOP"
                        self.recordingLabel.textColor = UIColor(hexString: "C4C4C4")
                        self.recordingButton.setImage(UIImage(named:"stop_recording"), for: .normal)
                        self.isRecording = true
                        self.startTime = Date().timeIntervalSinceReferenceDate
                        self.labelTimer = Timer.scheduledTimer(timeInterval: 0.01, target: self, selector: #selector(self.updateTime), userInfo: nil, repeats: true)
                        
                        DispatchQueue.main.async {
                            // start csv recording
                            if self.csvTimer == nil {
                                self.csvRecorder.startRecordingWithHeader(["time", "speed", "rpm", "lat", "lon"])
                                self.csvStartTime = DispatchTime.now()
                                let interval = 1.0/5.0
                                self.csvTimer = Timer.scheduledTimer(timeInterval: interval, target: self, selector: #selector(self.timerUpdate), userInfo: nil, repeats: true)
                            }
                            
                            UIView.animate(withDuration: 3.4,
                                           delay: 0,
                                           options: .curveEaseIn,
                                           animations: {
                                            let screenWdith = self.view.frame.size.width
                                            self.viewWidthConstraint.constant = (screenWdith/2)*0.995
                                            self.view.layoutIfNeeded()
                            }, completion: { (success) in
                                if self.viewWidthConstraint.constant != 0 {
                                    self.timer = Timer.scheduledTimer(timeInterval: 1/50, target: self, selector: #selector(self.runTimedCode), userInfo: nil, repeats: true)
                                }
                            })
                        }
                    })
                }
                
                // indicator
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.4, execute: {
                    self.activityIndicator.stopAnimating()
                })
                
                // car error
                if errorReason != nil {
                    self.showAlert("Car error", message: errorReason!)
                    return
                }
            }
        } else {
            activityIndicator.startAnimating()
            FLY360Helper.sharedInstance().stopRecording(self.connectedDevice!) { (error) in
                if let error = error {
                    self.showAlert("Error stopping recording", message: error.localizedDescription)
                } else {
                    self.isRecording = false
                    DispatchQueue.main.async {
                        self.recordingLabel.text = "REC"
                        self.recordingLabel.textColor = UIColor(hexString: "F34849")
                        self.recordingButton.setImage(UIImage(named:"start_recording"), for: .normal)
                    }
                    
                    self.removeAnimations()
                    
                    self.point.x = 0
                    
                    self.labelTimer?.invalidate()
                    self.labelTimer = nil
                    
                    self.timer?.invalidate()
                    self.timer = nil
                    
                    // stop timer
                    self.csvTimer?.invalidate()
                    self.csvTimer = nil
                    
                    // stop csv
                    let csvURL = self.csvRecorder.stopRecording()
                    
                    // media
                    DispatchQueue.main.asyncAfter(deadline: .now() + 4, execute: {
                        FLY360Helper.sharedInstance().getMediaList(self.connectedDevice!, withCompletionHandler: { (list, error) in
                            if let error = error {
                                self.showAlert("Error saving recording", message: error.localizedDescription)
                            } else {
                                guard let media = list?.first else {
                                    return
                                }
                                
                                // session
                                let session = Session()
                                session.csvName = csvURL.lastPathComponent
                                session.recordingName = media.name
                                session.date = Date()
                                
                                if let lat = LocationHelper.shared.myLocation?.coordinate.latitude, let long = LocationHelper.shared.myLocation?.coordinate.longitude {
                                    session.latitude = "\(lat)"
                                    session.longitude = "\(long)"
                                    
                                    LocationHelper.shared.reverseGeocodeLocation(completion: { (locality) in
                                        session.locality = locality
                                        
                                        // save
                                        SessionHelper.shared.add(session)
                                    })
                                } else {
                                    SessionHelper.shared.add(session)
                                }
                                
                                // Show Preview
                                DispatchQueue.main.async {
                                    let previewVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PreviewViewController") as! PreviewViewController
                                    previewVC.media = media as! FLYDeviceMedia
                                    previewVC.connectedDevice = self.connectedDevice
                                    self.navigationController?.pushViewController(previewVC, animated: true)
                                }
                            }
                            self.activityIndicator.stopAnimating()
                        })
                    })
                }
            }
        }
    }
    
    fileprivate func removeAnimations() {
        self.growingView.layer.removeAllAnimations()
        self.separatorView.layer.removeAllAnimations()
        if let layers = self.growingView.layer.sublayers, let layers2 = separatorView.layer.sublayers {
            for l in layers {
                l.removeAllAnimations()
            }
            for l in layers2 {
                l.removeAllAnimations()
            }
        }
    }
    
    @objc func runTimedCode() {
        guard isRecording else {
            return
        }
        self.point.x = self.point.x + 1
        UIView.animate(withDuration: 1/50) {
            self.collectionView.setContentOffset(self.point, animated: false)
        }
    }
    
    @objc func updateTime() {
        // Calculate total time since timer started in seconds
        var time = Date().timeIntervalSinceReferenceDate - startTime
        
        // Calculate minutes
        let minutes = UInt8(time / 60.0)
        time -= (TimeInterval(minutes) * 60)
        
        // Calculate seconds
        let seconds = UInt8(time)
        time -= TimeInterval(seconds)
        
        // Calculate milliseconds
        let milliseconds = UInt8(time * 100)
        
        // Format time vars with leading zero
        let strMinutes = String(format: "%02d", minutes)
        let strSeconds = String(format: "%02d", seconds)
        let strMilliseconds = String(format: "%02d", milliseconds)
        
        // Concatenate minuets, seconds and milliseconds as assign it to the UILabel
        minLabel.text = strMinutes
        secLabel.text = strSeconds
        msLabel.text = strMilliseconds
    }
    
    @objc func timerUpdate() {
        getVehicleStatusData()
    }
    
    @objc func getVehicleStatusData() {
        CarHelper.shared.getVehicleStatus { (status, error) in
            guard let statusData = status else {
                return
            }
            
            // settings
            var indexDiagnostics: Int
            var indexVehicleLocation: Int
            
            switch statusData.name! {
            case "Mission":
                indexDiagnostics = 2
                indexVehicleLocation = 18
            case "Cayenne":
                indexDiagnostics = 3
                indexVehicleLocation = 20
            default:
                indexDiagnostics = 0
                indexVehicleLocation = 0
            }
            
            var timestampStr, speedStr, rpmStr, latStr, lonStr:String
            
            // diagnostics
            if let diagnostics = statusData.states?[indexDiagnostics] as? Diagnostics, let location = statusData.states?[indexVehicleLocation] as? VehicleLocation {
                
                let end = DispatchTime.now()
                let nanoTime = end.uptimeNanoseconds - self.csvStartTime!.uptimeNanoseconds
                let timeInterval = Double(nanoTime) / 1_000_000_000
                
                timestampStr = String(format: "%.2f", timeInterval)
                speedStr = "\(diagnostics.speed ?? Int16.max)"
                rpmStr = "\(diagnostics.engineRPM ?? UInt16.max)"
                
                latStr = "\(location.coordinate?.latitude ?? 0)"
                lonStr = "\(location.coordinate?.longitude ?? 0)"
                
                self.csvRecorder.recordRow([timestampStr, speedStr, rpmStr, latStr, lonStr])
            } else {
                print("Error CSV Recording: Can't find data")
            }
        }
    }
    
    func showAlert(_ title: String, message: String) {
        // alert
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        // actions
        let actionOk = UIAlertAction(title: "OK", style: .default) { (action) in }
        alert.addAction(actionOk)
        
        // controller
        let window = UIApplication.shared.keyWindow
        if window != nil {
            let controller = window?.rootViewController
            controller?.present(alert, animated: true, completion: nil)
        }
    }
}

extension RecordingViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return MAX_ITEMS + 100
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "timerCell", for: indexPath) as! TimerCollectionViewCell
        let row = indexPath.row
        cell.setupCell(row: row)
        
        return cell
    }
}
