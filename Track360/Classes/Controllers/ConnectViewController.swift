//
//  ConnectViewController.swift
//  Track360
//
//  Created by Conjure on 13/04/2018.
//  Copyright © 2018 Conjure. All rights reserved.
//

import UIKit

class ConnectViewController: ScreenViewController {

    // MARK: - Outlets
    @IBOutlet weak var connectButton: UIButton!
    
    // MARK: - Initialization
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //debug
        #if DEBUG
            checkDebugSettings()
        #endif
    }
    
    // MARK: - Debug
    func checkDebugSettings() {
        if Debug.ScreenSkip > 0 {
            
            // storyboard
            let bundle = Bundle(for: ConnectViewController.self)
            let storyboard = UIStoryboard(name: "Main", bundle: bundle)

            // screen
            let screens = [
                "SelectCameraViewController",       //1
                "HomeViewController",               //2
                "RecordingViewController",          //3
                "GalleryViewController",            //4
            ]
            let screenIdentifier = screens[Debug.ScreenSkip - 1]
            let debugViewController = storyboard.instantiateViewController(withIdentifier: screenIdentifier)
            
            // show
            navigationController?.viewControllers = [debugViewController]
        }
    }
}
