//
//  SelectCameraViewController.swift
//  Track360
//
//  Created by Conjure on 13/04/2018.
//  Copyright © 2018 Conjure. All rights reserved.
//

import UIKit
import Fly360SDK

class SelectCameraViewController: ScreenViewController {

    // MARK: - Outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var videoButton: UIButton!
    @IBOutlet weak var skipButton: UIButton!
    
    // MARK: - Properties
    var deviceList: [FLYDiscoveredDevice]?
    var refresh: UIRefreshControl!
    
    // MARK: - Initialization
    override func viewDidLoad() {
        super.viewDidLoad()

        // title
        title = "Choose camera"
        
        // background
        if UIScreen.main.bounds.height < 812.0 {
            backgroundView.contentMode = .scaleAspectFill
        }
        
        // table setup
        tableView.register(CameraTableViewCell.self, forCellReuseIdentifier: CameraTableViewCell.defaultReuseIdentifier)
        tableView.separatorInset = UIEdgeInsetsMake(0, 40, 0, 40)
        tableView.delegate = self
        tableView.dataSource = self
        
        // refresh
        refresh = UIRefreshControl()
        refresh.addTarget(self, action: #selector(reloadTable), for: .valueChanged)
        tableView.refreshControl = refresh
        
        // debug
        skipButton.isHidden = true
        videoButton.isHidden = true
    }
    
    // MARK: - Lifecycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // reload
        if self.deviceList == nil {
            tableView.setContentOffset(CGPoint(x: 0, y: tableView.contentOffset.y - refresh.frame.size.height), animated: true)
            tableView.refreshControl?.beginRefreshing()
            reloadTable()
        }
    }
    
    @objc func reloadTable() {
        FLY360Helper.sharedInstance().getDiscoveredDevices { (devices, error) in
            if let error = error {
                print("Error discovering devices: \(error)")
            } else {
                if let list = devices {
                    self.deviceList = list
                    self.tableView.reloadData()
                }
            }
            self.tableView.refreshControl?.endRefreshing()
        }
    }
    
    // MARK: - IB Actions
    @IBAction func onVideo() {
        let videoURL = CameraHelper.shared.dummyVideoURL
        let playerViewController = FLY360SDK.sharedInstance().getPlayerFor(videoURL)
        present(playerViewController, animated: true, completion: nil)
    }
}

// MARK: - UITableViewDelegate
extension SelectCameraViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // data
        if let device = deviceList?[indexPath.row] {
            CameraHelper.shared.discoveredDevice = device
        }
        
        // screen
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            let nextViewController = HomeViewController.loadViewController()
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
    }
}

// MARK: - UITableViewDataSource
extension SelectCameraViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return deviceList == nil ? 0 : deviceList!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // cell
        let cell = tableView.dequeueReusableCell(withIdentifier: CameraTableViewCell.defaultReuseIdentifier) as! CameraTableViewCell
        
        // update
        if let device = deviceList?[indexPath.row] {
            cell.textLabel?.text = device.name
        }
        
        return cell
    }
}
