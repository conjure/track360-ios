//
//  GalleryViewController.swift
//  Track360
//
//  Created by Conjure on 17/04/2018.
//  Copyright © 2018 Conjure. All rights reserved.
//

import UIKit
import Fly360SDK

class GalleryViewController: ScreenViewController {

    // MARK: - Outlets
    @IBOutlet weak var filterView: FilterView!
    @IBOutlet weak var tableview: UITableView!

    // MARK: - Properties
    var discoveredDevice: FLYDiscoveredDevice?
    fileprivate var connectedDevice: FLYConnectedDevice!
    var mediaList:[FLYDeviceMedia] = []
    var thumbnailCache:NSMutableDictionary!
    var recordingSessions:[Session]?
    
    // MARK: - Initialization
    override func viewDidLoad() {
        super.viewDidLoad()

        // title
        title = "Gallery"
        
        // tabs
        filterView.view.backgroundColor = .black
        filterView.itemSpacing = 0
        filterView.margin = 0
        filterView.dataSource = self
        filterView.selectionCallback = { [weak self] (index: Int) in
            self?.filterViewDidSelectItem(index)
        }
        filterView.scrollView.isScrollEnabled = false
        
        self.thumbnailCache = NSMutableDictionary()
        self.tableview.refreshControl = UIRefreshControl()
        self.tableview.refreshControl?.addTarget(self, action: #selector(reloadMedia), for: .valueChanged)
        self.tableview.register(UINib(nibName: "GalleryTableViewCell", bundle: nil), forCellReuseIdentifier: "galleryCell")
        
        self.recordingSessions = SessionHelper.shared.getAllSessions()
    }
    
    // MARK: - Layout
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        filterView.reloadData()
    }

    // MARK: - Lifecycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        // debug
        let sessions = SessionHelper.shared.getAllSessions()
        print(sessions ?? "")
        
        self.tableview.setContentOffset(CGPoint(x: 0, y: self.tableview.contentOffset.y - (self.tableview.refreshControl?.frame.size.height)!), animated: true)
        self.tableview.refreshControl?.beginRefreshing()
        
        if discoveredDevice != nil {
            FLY360Helper.sharedInstance().connect(to: discoveredDevice!) { (connectedDevice, error) in
                if let error = error {
                    print("Connection Error \(error.localizedDescription)")
                    return
                } else {
                    if let connected = connectedDevice {
                        self.connectedDevice = connected
                        
                        self.reloadMedia()
                    }
                }
            }
        }
    }
    
    // MARK: - Helpers
    @objc func reloadMedia() {
        FLY360Helper.sharedInstance().getMediaList(self.connectedDevice!) { (list, error) in
            if let error = error {
                print("Error finding recordings: \(error)")
            } else if let list = list {
                if list.count != self.mediaList.count {
                    let recordings = list as! [FLYDeviceMedia]
                    if let sessions = self.recordingSessions {
                        self.mediaList = recordings.filter({ (media) -> Bool in
                            for session in sessions {
                                if session.recordingName == media.name {
                                    return true
                                }
                            }
                            return false
                        })
                    }
                    
                    self.thumbnailCache.removeAllObjects()
                    self.tableview.reloadData()
                }
            }
            self.tableview.refreshControl?.endRefreshing()
        }
    }
    
    func downloadVideo(video:FLYDeviceMedia) {
        JustHUD.shared.showInView(view: view, withHeader: "Downloading", andFooter: "Please wait...")
        
        UIApplication.shared.beginIgnoringInteractionEvents()
        
        let arr = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        if let documentsUrl = arr.first {
            let fileName = documentsUrl.appendingPathComponent(video.downloadURL.lastPathComponent)
            
            FLY360Helper.sharedInstance().downloadVideo(self.connectedDevice!, video: video, toLocation: fileName, withCompletionHandler: { (error) in
                if let error = error {
                    print("Error downloading video: \(error)")
                } else {
                    print("Downloaded \(fileName.lastPathComponent) - \(Date())")
                    
                    DispatchQueue.main.async {
                        JustHUD.shared.hide()
                    }
                }
            })
        }
    }


    // MARK: - Callbacks
    func filterViewDidSelectItem(_ index: Int) {

    }
}

// MARK: - FilterViewDataSource
extension GalleryViewController: FilterViewDataSource {

    func numberOfItems(filterView: FilterView) -> Int {
        return 2
    }

    func itemAt(index: Int, filterView: FilterView) -> FilterViewItem {
        // item
        let title = index == 0 ? "ON FLY 360" : "ON PHONE"
        let item = TabFilterViewItem(tabTitle: title)
        item.view.backgroundColor = .black

        // size
        let width = filterView.frame.width * 0.5
        item.frame = CGRect(x: 0, y: 0, width: width, height: filterView.bounds.height)

        return item
    }
}

extension GalleryViewController: UITableViewDelegate, UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        return self.mediaList.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableview.dequeueReusableCell(withIdentifier: "galleryCell") as! GalleryTableViewCell

        let media = self.mediaList[indexPath.section]
        cell.updateCell(media: media)
        cell.buttonCallback = { [weak self] (video: FLYDeviceMedia) in
            self?.downloadVideo(video: video)
        }

        if thumbnailCache.object(forKey: indexPath) == nil {
            FLY360Helper.sharedInstance().generateThumbnailImage(self.connectedDevice!, video: media, size: (cell.thumbnailImageView.frame.size as NSValue) as! CGSize, withCompletionHandler: { (image, error) in
                if let error = error {
                    print("Error generating thumbnail image for cell: \(error)")
                } else {
                    if let image = image {
                        self.thumbnailCache.setObject(image, forKey: indexPath as NSCopying)
                        DispatchQueue.main.async {
                            cell.thumbnailImageView.image = image
                        }
                    }
                }
            })
        } else {
            if let image = self.thumbnailCache.object(forKey: indexPath) as? UIImage {
                DispatchQueue.main.async {
                    cell.thumbnailImageView.image = image
                }
            }
        }

        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 262.0
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 20.0
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
}
