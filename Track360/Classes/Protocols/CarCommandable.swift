//
//  CarCommand.swift
//  Track360
//
//  Created by Conjure on 09/03/2018.
//  Copyright © 2018 Conjure. All rights reserved.
//

import Foundation

protocol CarCommandable {
    associatedtype ParsedData
    
    var bytes: [UInt8] { get }
    func parseData(_ data: Any)
    func getParsedData() -> ParsedData?
}
