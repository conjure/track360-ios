//
//  Reusable.swift
//  Track360
//
//  Created by Conjure on 16/04/2018.
//  Copyright © 2018 Conjure. All rights reserved.
//

import Foundation

protocol Reusable {
    static var defaultReuseIdentifier: String { get }
}

extension Reusable {
    static var defaultReuseIdentifier: String {
        return String(describing: self)
    }
}
