//
//  UIViewController+Storyboard.swift
//  TFLHealth
//
//  Created by Conjure on 10/04/2018.
//  Copyright © 2018 Conjure. All rights reserved.
//

import UIKit

extension UIViewController {
    
    class func loadViewController() -> UIViewController {
        let className = String(describing: self)
        let bundle = Bundle(for: self)
        let storyboard = UIStoryboard(name: "Main", bundle: bundle)
        return storyboard.instantiateViewController(withIdentifier: className)
    }
}
