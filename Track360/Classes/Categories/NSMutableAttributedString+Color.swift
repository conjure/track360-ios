//
//  NSMutableAttributedString+Color.swift
//  BBTours
//
//  Created by Conjure on 25/10/2017.
//  Copyright © 2017 Conjure. All rights reserved.
//

import UIKit

extension NSMutableAttributedString {
    
    func setColor(color: UIColor, forText stringValue: String) {
        let range: NSRange = self.mutableString.range(of: stringValue, options: .caseInsensitive)
        self.addAttribute(.foregroundColor, value: color, range: range)
    }
}
