//
//  UIFont+Bundle.swift
//  Track360
//
//  Created by Conjure on 17/04/2018.
//  Copyright © 2018 Conjure. All rights reserved.
//

import UIKit

public extension UIFont {
    
    class func loadFonts(_ fonts: [String], bundleIdentifierString: String) {
        for fontName in fonts {
            registerFontWithFilenameString(filenameString: fontName, bundleIdentifierString: bundleIdentifierString)
        }
    }
    
    static func registerFontWithFilenameString(filenameString: String, bundleIdentifierString: String) {
        if let frameworkBundle = Bundle(identifier: bundleIdentifierString) {
            let pathForResourceString = frameworkBundle.path(forResource: filenameString, ofType: nil)
            let fontData = NSData(contentsOfFile: pathForResourceString!)
            let dataProvider = CGDataProvider(data: fontData!)
            let fontRef = CGFont(dataProvider!)
            var errorRef: Unmanaged<CFError>? = nil
            
            if fontRef != nil {
                if (CTFontManagerRegisterGraphicsFont(fontRef!, &errorRef) == false) {
                    print("Failed to register font - register graphics font failed - this font may have already been registered in the main bundle.")
                }
            }
        }
        else {
            print("Failed to register font - bundle identifier invalid.")
        }
    }
}
