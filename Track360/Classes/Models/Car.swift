//
//  Car.swift
//  Track360
//
//  Created by Conjure on 08/03/2018.
//  Copyright © 2018 Conjure. All rights reserved.
//

import Foundation

struct Car {
    var name: String?
    var modelName: String?
    var powerTrain: String?
}
