//
//  Session.swift
//  Track360
//
//  Created by Conjure on 12/03/2018.
//  Copyright © 2018 Conjure. All rights reserved.
//

import Foundation
import RealmSwift

class Session: Object {
    @objc dynamic var csvName: String?
    @objc dynamic var recordingName: String?
    @objc dynamic var date: Date?
    @objc dynamic var latitude: String?
    @objc dynamic var longitude: String?
    @objc dynamic var locality: String?
    @objc dynamic var isLocalCopy = false
    
    override static func primaryKey() -> String? {
        return "csvName"
    }
}
