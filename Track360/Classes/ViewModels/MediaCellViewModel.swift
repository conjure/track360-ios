//
//  MediaCellViewModel.swift
//  Track360
//
//  Created by Dinesh Vijaykumar on 08/03/2018.
//  Copyright © 2018 Conjure. All rights reserved.
//

import Foundation
import FlyDeviceSDK

class MediaCellViewModel {
    
    var media: FLYDeviceMedia!
    var dateFormatter = DateFormatter()
    
    init(media: FLYDeviceMedia) {
        self.media = media
        self.dateFormatter.dateFormat = "dd/MM/yyyy HH:mm"
    }
    
    var title: String {
        return media.name
    }
    
    var date: String {
        return dateFormatter.string(from: media.modificationDate)
    }
    
    var duration: String {
        return FLYMedia.string(for: media.duration)
    }
}
