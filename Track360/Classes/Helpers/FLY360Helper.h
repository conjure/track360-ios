//
//  FLY360Helper.h
//  Track360
//
//  Created by Conjure on 16/03/2018.
//  Copyright © 2018 Conjure. All rights reserved.
//

#import <UIKit/UIKit.h>

@class FLYDiscoveredDevice, FLYConnectedDevice, FLYMedia, FLYDeviceMedia;

@interface FLY360Helper : NSObject

+ (FLY360Helper *_Nonnull)sharedInstance;

- (void)getDiscoveredDevices:(void(^ _Nonnull)(NSArray<FLYDiscoveredDevice *> *_Nullable, NSError *_Nullable error ))handler;

- (void)connectToDevice:(FLYDiscoveredDevice *_Nonnull)device withCompletionHandler:(void(^_Nonnull)(FLYConnectedDevice *_Nullable, NSError * _Nullable error))handler;

- (void)startRecording:(FLYConnectedDevice *_Nonnull)device withCompletionHandler:(void(^ _Nonnull)(NSError *_Nullable error ))handler;

- (void)stopRecording:(FLYConnectedDevice *_Nonnull)device withCompletionHandler:(void(^ _Nonnull)(NSError *_Nullable error ))handler;

- (void)getMediaList:(FLYConnectedDevice *_Nonnull)device withCompletionHandler:(void(^ _Nonnull)(NSArray<FLYMedia *> * _Nullable mediaList, NSError * _Nullable error))handler;

- (void)downloadVideo:(FLYConnectedDevice *_Nonnull)device video:(FLYMedia *_Nonnull)media toLocation:(NSURL *_Nonnull)location withCompletionHandler:(void(^ _Nonnull)(NSError *_Nullable error ))handler;

- (void)generateThumbnailImage:(FLYConnectedDevice *_Nonnull)device video:(FLYDeviceMedia *_Nonnull)media size:(CGSize)size withCompletionHandler:(void(^ _Nonnull)(UIImage * _Nullable thumbnail, NSError * _Nullable error))handler;

@end
