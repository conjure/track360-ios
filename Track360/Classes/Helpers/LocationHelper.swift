//
//  LocationHelper.swift
//  Track360
//
//  Created by Dinesh Vijaykumar on 12/03/2018.
//  Copyright © 2018 Conjure. All rights reserved.
//

import Foundation
import CoreLocation

class LocationHelper: NSObject {
    
    // MARK: - Singleton
    static let shared = LocationHelper()
    
    // MARK: - Properties
    let locationManager = CLLocationManager()
    var myLocation: CLLocation?
    
    // MARK: - Initialization
    override init() {
        super.init()
        
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        }
    }
    
    // MARK: - Lifecycle
    func startUpdating() {
        locationManager.startUpdatingLocation()
    }
    
    func stopUpdating() {
        locationManager.stopUpdatingLocation()
    }
    
    func reverseGeocodeLocation(completion: @escaping (String?) -> ()) {
        guard let myLocation = myLocation else {
            completion(nil)
            return
        }
        
        CLGeocoder().reverseGeocodeLocation(myLocation) { (placemarks, error) in
            if error != nil {
                print("Reverse geocoder failed with error" + error!.localizedDescription)
                return
            }
            
            if let placemarks = placemarks, !placemarks.isEmpty {
                let placemark = placemarks[0]
                completion(placemark.locality)
            }
        }
    }
}

// MARK: - CLLocationManagerDelegate
extension LocationHelper: CLLocationManagerDelegate {
    
    public func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedAlways || status == .authorizedWhenInUse {
            startUpdating()
        } else {
            stopUpdating()
        }
    }
    
    public func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.first else {
            return
        }
        
        myLocation = location
    }
}
