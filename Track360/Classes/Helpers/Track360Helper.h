//
//  Track360Helper.h
//  Track360
//
//  Created by Conjure on 23/03/2018.
//  Copyright © 2018 Conjure. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol Track360HelpereDelegate;

/*
 *  Only for use with framework target.
 */
@interface Track360Helper : NSObject {
    id <Track360HelpereDelegate> __weak delegate;
}
@property (nonatomic, weak, nullable) id <Track360HelpereDelegate> delegate;

+ (Track360Helper *_Nonnull)sharedInstance;

- (UIViewController *_Nonnull)getAppViewController;
- (void)startTrack360;
- (void)stopTrack360;
- (void)sendMessage:(NSString *_Nonnull)objectName method:(NSString *_Nonnull)methodName andValue:(NSString *_Nonnull)value;

@end


@protocol Track360HelpereDelegate <NSObject>
@optional
- (void)track360AppDidStart:(Track360Helper *_Nonnull)trackHelper;
- (void)track360AppDidFinish:(Track360Helper *_Nonnull)trackHelper;
- (void)track360App:(Track360Helper *_Nonnull)trackHelper didSendMessage:(NSString *_Nonnull)objectName method:(NSString *_Nonnull)methodName andValue:(NSString *_Nonnull)value;
@end
