//
//  SessionHelper.swift
//  Track360
//
//  Created by Conjure on 12/03/2018.
//  Copyright © 2018 Conjure. All rights reserved.
//

import Foundation
import RealmSwift

public class SessionHelper {
    
    // MARK: - Singleton
    public static let shared = SessionHelper()
    
    // MARK: - Lifecycle
    func add(_ session: Session) {
        let realm = try! Realm()
        
        try! realm.write {
            realm.add(session)
        }
    }
    
    func update(_ completionHandler: (() -> Void)) {
        let realm = try! Realm()
        try! realm.write {
            completionHandler()
        }
    }
    
    // MARK: - Setters / Getters
    func getAllSessions() -> [Session]? {
        let realm = try! Realm()
        let objects = realm.objects(Session.self)
        
        var sessions = [Session]()
        for session in objects {
            sessions.append(session)
        }
        
        return sessions
    }
}
