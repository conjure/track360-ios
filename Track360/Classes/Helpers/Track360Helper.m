//
//  Track360Helper.m
//  Track360
//
//  Created by Conjure on 23/03/2018.
//  Copyright © 2018 Conjure. All rights reserved.
//

#import "Track360Helper.h"

@implementation Track360Helper

@synthesize delegate;

+ (Track360Helper *_Nonnull)sharedInstance {
    static Track360Helper *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[Track360Helper alloc] init];
    });
    
    return sharedInstance;
}

- (UIViewController *_Nonnull)getAppViewController {
    NSBundle *bundle = [NSBundle bundleForClass:[Track360Helper class]];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:bundle];
    UIViewController *viewController = [storyboard instantiateInitialViewController];
    return viewController;
}

- (void)startTrack360 {
    if ([delegate respondsToSelector:@selector(track360AppDidStart:)]) {
        [delegate track360AppDidStart:self];
    }
}

- (void)stopTrack360 {
    if ([delegate respondsToSelector:@selector(track360AppDidFinish:)]) {
        [delegate track360AppDidFinish:self];
    }
}

- (void)sendMessage:(NSString *_Nonnull)objectName method:(NSString *_Nonnull)methodName andValue:(NSString *_Nonnull)value {
    if ([delegate respondsToSelector:@selector(track360App:didSendMessage:method:andValue:)]) {
        [delegate track360App:self didSendMessage:objectName method:methodName andValue:value];
    }
}

@end
