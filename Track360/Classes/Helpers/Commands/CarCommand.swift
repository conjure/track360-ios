//
//  CarCommand.swift
//  Track360
//
//  Created by Conjure on 09/03/2018.
//  Copyright © 2018 Conjure. All rights reserved.
//

import Foundation
import AutoAPI

class CarCommand: CarCommandable {

    // MARK: - Properties
    typealias ParsedData = Car
    var data: Any?
    
    // MARK: - Lifecycle
    var bytes: [UInt8] {
        let bytes = VehicleStatus.getVehicleStatus
        return bytes
    }
    
    // MARK: - Lifecycle
    func parseData(_ data: Any) {
        self.data = data
    }
    
    // MARK: - Setters / Getters
    func getParsedData() -> Car? {
        guard let vehicleStatus = data as? VehicleStatus else {
            return nil
        }
        
        // car
        let powertrainType = getPowerTrain(vehicleStatus.powerTrain!)
        let car = Car(name: vehicleStatus.name, modelName: vehicleStatus.modelName, powerTrain: powertrainType)
        
        return car
    }
    
    private func getPowerTrain(_ powerTrainType: PowerTrain) -> String {
        switch powerTrainType {
        case .allElectric:
            return "Electric"
        case .combustionEngine:
            return "Combustion"
        default:
            return "Other"
        }
    }
}
