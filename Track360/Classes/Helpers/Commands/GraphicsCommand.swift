//
//  GraphicsCommand.swift
//  Track360
//
//  Created by Dinesh Vijaykumar on 23/03/2018.
//  Copyright © 2018 Conjure. All rights reserved.
//

import Foundation
import AutoAPI

class GraphicsCommand: CarCommandable {
    
    // MARK: - Properties
    typealias ParsedData = Graphics
    var data: Any?
    var url: URL!
    
    // MARK: - Lifecycle
    var bytes: [UInt8] {
        let bytes = Graphics.displayImage(url)
        return bytes
    }
    
    // MARK: - Lifecycle
    func parseData(_ data: Any) {
        self.data = data
    }
    
    // MARK: - Setters / Getters
    func getParsedData() -> Graphics? {
        guard let graphics = data as? Graphics else {
            return nil
        }
        
        return graphics
    }
    
    
}

