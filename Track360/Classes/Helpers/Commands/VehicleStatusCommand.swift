//
//  VehicleStatusCommand.swift
//  Track360
//
//  Created by Conjure on 09/03/2018.
//  Copyright © 2018 Conjure. All rights reserved.
//

import Foundation
import AutoAPI

class VehicleStatusCommand: CarCommandable {
    
    // MARK: - Properties
    typealias ParsedData = VehicleStatus
    var data: Any?
    
    // MARK: - Lifecycle
    var bytes: [UInt8] {
        let bytes = VehicleStatus.getVehicleStatus
        return bytes
    }
    
    // MARK: - Lifecycle
    func parseData(_ data: Any) {
        self.data = data
    }
    
    // MARK: - Setters / Getters
    func getParsedData() -> VehicleStatus? {
        guard let vehicleStatus = data as? VehicleStatus else {
            return nil
        }
        
        // settings
        var indexDiagnostics: Int
        var indexVehicleLocation: Int
        
        switch vehicleStatus.name! {
        case "Mission":
            indexDiagnostics = 2
            indexVehicleLocation = 21
        case "Cayenne":
            indexDiagnostics = 3
            indexVehicleLocation = 23
        default:
            indexDiagnostics = 0
            indexVehicleLocation = 0
        }
        
        // diagnostics
        if let diagnostics = vehicleStatus.states?[indexDiagnostics] as? Diagnostics {
            let timestamp = Date()
            let timestampStr = "Car - TS: \(timestamp)"
            let speedStr = " | speed: \(diagnostics.speed ?? Int16.max)"
            let rpmStr = " | rpm: \(diagnostics.engineRPM ?? UInt16.max)"
            let oilStr = " | oil: \(diagnostics.engineOilTemperature ?? Int16.max)C"
            
            print(timestampStr + speedStr + rpmStr + oilStr)
        }
        
        // location
        if let location = vehicleStatus.states?[indexVehicleLocation] as? VehicleLocation {
            if let coordinates = location.coordinate {
                let timestamp = Date()
                let timestampStr = "Car - TS: \(timestamp)"
                let coordinatesStr = " | lat: \(coordinates.latitude) long: \(coordinates.longitude)"
                
                print(timestampStr + coordinatesStr)
            }
            
        }
        
        return vehicleStatus
    }
}
