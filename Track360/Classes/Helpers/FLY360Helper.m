//
//  FLY360Helper.m
//  Track360
//
//  Created by Conjure on 16/03/2018.
//  Copyright © 2018 Conjure. All rights reserved.
//

#import "FLY360Helper.h"
#import <FLY360SDK/FLY360SDK.h>

@implementation FLY360Helper

+ (FLY360Helper *_Nonnull)sharedInstance {
    static FLY360Helper *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[FLY360Helper alloc] init];
    });
    
    return sharedInstance;
}

- (void)getDiscoveredDevices:(void(^ _Nonnull)(NSArray<FLYDiscoveredDevice *> *_Nullable, NSError *_Nullable error ))handler {
    [[FLY360SDK sharedInstance] getDiscoveredDeviceListWithCompletionHandler:^(NSArray<FLYDiscoveredDevice *> *array, NSError *error) {
        
        return handler(array, error);
    } forDuration:4];
}

- (void)connectToDevice:(FLYDiscoveredDevice *_Nonnull)device withCompletionHandler:(void(^_Nonnull)(FLYConnectedDevice *_Nullable, NSError * _Nullable error))handler {
    
    [[FLY360SDK sharedInstance] connectToDiscoveredDevice:device withCompletionHandler:^(FLYConnectedDevice *device, NSError *error) {
        return handler(device, error);
    }];
}

- (void)startRecording:(FLYConnectedDevice *_Nonnull)device withCompletionHandler:(void(^ _Nonnull)(NSError *_Nullable error ))handler {
    [device startRecordWithCompletionHandler:^(NSError * _Nonnull error) {
        NSError *deviceError = nil;
        if (error != nil) {
            deviceError = error;
        }
        
        return handler(deviceError);
    }];
}

- (void)stopRecording:(FLYConnectedDevice *_Nonnull)device withCompletionHandler:(void(^ _Nonnull)(NSError *_Nullable error ))handler {
    [device stopRecordWithCompletionHandler:^(NSError * _Nonnull error) {
        return handler(error);
    }];
}

- (void)getMediaList:(FLYConnectedDevice *_Nonnull)device withCompletionHandler:(void(^ _Nonnull)(NSArray<FLYMedia *> * _Nullable mediaList, NSError * _Nullable error))handler {
    [device getMediaListWithCompletionHandler:^(NSArray<FLYMedia *> * _Nonnull mediaList, NSError * _Nonnull error) {
        return handler(mediaList, error);
    }];
}

- (void)downloadVideo:(FLYConnectedDevice *_Nonnull)device video:(FLYMedia *_Nonnull)media toLocation:(NSURL *_Nonnull)location withCompletionHandler:(void(^ _Nonnull)(NSError *_Nullable error ))handler {
    [device downloadMediaFrom360Fly:media toLocation:location withCompletionHandler:^(NSError * _Nonnull error) {
        return handler(error);
    }];
}

- (void)generateThumbnailImage:(FLYConnectedDevice *_Nonnull)device video:(FLYDeviceMedia *_Nonnull)media size:(CGSize)size withCompletionHandler:(void(^ _Nonnull)(UIImage * _Nullable thumbnail, NSError * _Nullable error))handler {
    [device generateThumbnailImageForMedia:media withSize:size withCompletionHandler:^(UIImage * _Nonnull thumbnail, NSError * _Nonnull error) {
        return handler(thumbnail, error);
    }];
}

@end
