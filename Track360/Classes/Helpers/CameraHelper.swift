//
//  CameraHelper.swift
//  Track360
//
//  Created by Conjure on 16/04/2018.
//  Copyright © 2018 Conjure. All rights reserved.
//

import Foundation
import Fly360SDK

public class CameraHelper {
    
    // MARK: - Singleton
    public static let shared = CameraHelper()
    
    // MARK: - Properties
    var discoveredDevice: FLYDiscoveredDevice?
    
    // MARK: - Setters / Getters
    var dummyVideoURL: URL {
        let url = Bundle.main.url(forResource: "drift_video_360.mp4", withExtension: nil)
        let reachableURL = try! reachableURLForEmbeddedFile(fileURL: url!)
        return reachableURL
    }
    
    // MARK: - Support
    //https://stackoverflow.com/questions/24882834/wkwebview-not-loading-local-files-under-ios-8
    func reachableURLForEmbeddedFile(fileURL: URL) throws -> URL {
        // Some safety checks
        if !fileURL.isFileURL {
            throw NSError(
                domain: "ReadOnlyResourceDomain",
                code: 1001,
                userInfo: [NSLocalizedDescriptionKey: NSLocalizedString("URL must be a file URL.", comment:"")])
        }
        _ = try! fileURL.checkResourceIsReachable()
        
        // Create "/temp/video" directory
        let fm = FileManager.default
        let tmpDirURL = URL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent("video")
        try! fm.createDirectory(at: tmpDirURL, withIntermediateDirectories: true, attributes: nil)
        
        // Now copy given file to the temp directory
        let dstURL = tmpDirURL.appendingPathComponent(fileURL.lastPathComponent)
        if !fm.fileExists(atPath: dstURL.path) {
            let _ = try? fm.removeItem(at: dstURL)
            try! fm.copyItem(at: fileURL, to: dstURL)
        }
        
        // Files in "/temp/video" load flawlesly :)
        return dstURL
    }
}
