//
//  CarHelper.swift
//  Track360
//
//  Created by Conjure on 09/03/2018.
//  Copyright © 2018 Conjure. All rights reserved.
//

import Foundation
import AutoAPI
import HMKit

public enum ConnectionMethod {
    case bluetooth
    case telematics
}

public enum ConnectionState {
    case unavailable
    case disconnected
    case idle
    case searching
    case connected
    case authenticated
}

public class CarHelper {
    
    // MARK: - Singleton
    public static let shared = CarHelper()
    
    // MARK: - Properties
    public var activeVehicleSerial: Data?
    public var connectionMethod: ConnectionMethod
    public var connectionState: ConnectionState
    
    // MARK: - Initialization
    private init() {
        // settings
        activeVehicleSerial = nil
        connectionMethod = .telematics
        connectionState = .unavailable
        
        //LocalDevice things
        LocalDevice.loggingOptions = [.command, .error, .general, .bluetooth, .urlRequests]
        LocalDevice.shared.delegate = self
    }
    
    // MARK: - Lifecycle
    func connect(completionHandler: @escaping ((_ car: Car?, _ failureReason: String?) -> Void)) {
        // LocalDevice represents THIS device in HMKit
        initialiseLocalDevice()
        
        // Telematics connects to the car / emulator through the internet, not Bluetooth (like the LocalDevice does)
        initialiseTelematics { (failureReason) in
            if failureReason == nil {
                // car
                self.getCarData(completionHandler: { (car, error) in
                    return completionHandler(car, error)
                })
            } else {
                // error
                return completionHandler(nil, failureReason)
            }
        }
    }
    
    // MARK: - Setters / Getters
    func getCarData(completionHandler: @escaping ((_ car: Car?, _ failureReason: String?) -> Void)) {
        let command = CarCommand()
        sendTelematicsCommand(command) { (returnCommand, filure) in
            let car = returnCommand?.getParsedData()
            return completionHandler(car, filure)
        }
    }
        
    func getVehicleStatus(completionHandler: @escaping ((_ status: VehicleStatus?, _ failureReason: String?) -> Void)) {
        let command = VehicleStatusCommand()
        sendTelematicsCommand(command) { (returnCommand, failureReason) in
            let status = returnCommand?.getParsedData()
            return completionHandler(status, nil)
        }
    }
    
    func sendGraphic(imageUrl:URL? = nil,completionHandler: @escaping ((_ status: Graphics?, _ failureReason: String?) -> Void)) {
        let command = GraphicsCommand()
        if let imageUrl = imageUrl {
            command.url = imageUrl
        } else {
            command.url = URL(string: "https://o.aolcdn.com/images/dims3/GLOB/legacy_thumbnail/800x450/format/jpg/quality/85/http://o.aolcdn.com/hss/storage/midas/ff3d6cac80cdc371ddc1bf367e7b3ae/200030487/patrick-long-918-pov.jpg")!
        }
        
        sendTelematicsCommand(command) { (returnedCommand, failureReason) in
            let status = returnedCommand?.getParsedData()
            return completionHandler(status, nil)
        }
    }
}

// MARK: - Commands
extension CarHelper {
    
    func sendTelematicsCommand<C: CarCommandable>(_ command: C, completionHandler: @escaping (( _ completed: C?, _ failureReason: String?) -> Void)) {
        guard let vehicleSerial = activeVehicleSerial else {
            return completionHandler(nil, "Missing Access Certificate for Telematics")
        }
        
        //execute
        do {
            try Telematics.sendCommand(command.bytes, serial: vehicleSerial) {
                switch $0 {
                case .failure(let failure):
                    print(failure)
                    completionHandler(nil, failure)
                    
                case .success(let data):
                    guard let commandResponse = data else {
                        return completionHandler(nil, "Missing command response data")
                    }
                    
                    // Only handles VALID AutoAPI responses
                    guard let response = AutoAPI.parseBinary(commandResponse) else {
                        return completionHandler(nil, "Can't parse commande response data")
                    }
                    
                    command.parseData(response)
                    
                    return completionHandler(command, nil)
                }
            }
        }
        catch {
            print(error)
            return completionHandler(nil, error.localizedDescription)
        }
    }
}

// MARK: - MHKit
extension CarHelper {
    
    func initialiseLocalDevice() {
        do {
            try LocalDevice.shared.initialise(
                deviceCertificate: "dGVzdMcfddtNHKdkESaPf41nQmpTPpHrDwMzTVqIzi31hYLNbZGuzglEyV1AQMnr104Vxxu+dCJvm3V7r4AT5FC6pyDq0gIWA+VL7q0gEJ3DwTJRL/AEy2WAwfIsTaRb0C2kQTeQrVKqcbhdQSaWycZ83vP9o8PoXYJCJPqSQUd9HS7HVXCXWE3kdXy4WiYhx8DPQqrlIx25",
                devicePrivateKey: "K7wkudgiPHPtajD/jBQhC6o4pWRptoGuUz4yvm+VEUM=",
                issuerPublicKey: "aT4jsqaQir6gECKJIPww4vGRPcHaIS5g7JaTjcQWAr0WzZ0IyzTBFVoqL1DrqZ2jybHteRP3voeiGnqEKDEDvg=="
            )
        }
        catch {
            // Handle the error
            print("Invalid initialisation parameters, please double check the snippet.")
        }
        
        
        // This just checks if you've seen the above (and are able to follow instructions)
        guard LocalDevice.shared.certificate != nil else {
            fatalError("Please initialise the HMKit with the instrucions above, thanks")
        }
    }
    
    func initialiseTelematics(completionHandler: @escaping (( _ failureReason: String?) -> Void)) {
        
        // Cayenne
        let accessToken: String = "xL8iwQHjSMzmEbtAz2ikwjK0HsVIyT6axgCrao-9pPsSx0nYFH1N8LzLvdhw0khhAFv4bZynR69hpM4hUc75jT3ZBxqj3oCeVAVG0ghscE1yrUuaJpqmvtsnK4cH9-bHKQ"
        
        // Mission
       // let accessToken: String = "tW5JulcVbstQr0BQ7vXQ3IGfaDW7BXvKhMtAClEq0MAtXp7smpZBqkEHlzLhuzNrQAizqxjZ_hL4ULTm84WW0TTW41SkIADnhdcoQm6ClEQsxOlC52U7K2wX2TcA5lnvzQ"
        
        
        do {
            try Telematics.downloadAccessCertificate(accessToken: accessToken) {
                switch $0 {
                case .failure(let failureReason):
                    print("Failed to download Access Certificate for Telematics: \(failureReason)")
                    return completionHandler(failureReason)
                case .success(let vehicleSerial):
                    // Set the serial to the Car.framework (the magical helper)
                    self.activeVehicleSerial = vehicleSerial
                    self.connectionState = .connected
                    return completionHandler(nil)
                }
            }
        }
        catch {
            print("Failed to download Access Certificate for Telematics: \(error)")
            return completionHandler(error.localizedDescription)
        }
    }
}

// MARK: - LocalDeviceDelegate
extension CarHelper: LocalDeviceDelegate {
    
    public func localDevice(didLoseLink link: Link) {
        link.delegate = nil
        
        // This gives time for HMKit to start it's broadcasting again (that it does automatically after losing a link),
        // so we can actually stop it.
        DispatchQueue.main.asyncAfter(deadline: (DispatchTime.now() + 0.01)) {
            self.connectionState = .disconnected
            
            if LocalDevice.shared.state == .broadcasting {
                LocalDevice.shared.stopBroadcasting()
            }
        }
    }
    
    public func localDevice(didReceiveLink link: Link) {
        link.delegate = self
        
        connectionState = .connected
        
        if LocalDevice.shared.state == .broadcasting {
            LocalDevice.shared.stopBroadcasting()
        }
    }
    
    public func localDevice(stateChanged state: LocalDeviceState, oldState: LocalDeviceState) {
        switch (state, oldState) {
        case (.bluetoothUnavailable, _):
            connectionState = .unavailable
            
        case (.broadcasting, _):
            connectionState = .searching
            
        case (.idle, _):
            connectionState = .idle
        }
    }
}

// MARK: - LinkDelegate
extension CarHelper: LinkDelegate {
    
    public func link(_ link: Link, authorisationRequestedBy serialNumber: [UInt8], approve: @escaping (() throws -> Void), timeout: TimeInterval) {
        do {
            try approve()
        }
        catch {
            print("\(type(of: self)) -\(#function) failed to approve authorisation: \(error)")
            
            link.dropLink()
            
            if LocalDevice.shared.state == .broadcasting {
                LocalDevice.shared.stopBroadcasting()
            }
        }
    }
    
    public func link(_ link: Link, commandReceived bytes: [UInt8]) {
        print("DATA:", bytes.map { String(format: "%02X", $0) }.joined())
        
        // Only handles VALID AutoAPI responses
        guard let response = AutoAPI.parseBinary(bytes) else {
            return
        }
        print(response)
    }
    
    public func link(_ link: Link, stateChanged oldState: LinkState) {
        switch (link.state, oldState) {
        case (.authenticated, .connected):
            DispatchQueue.main.asyncAfter(deadline: (DispatchTime.now() + 0.01)) {
                self.connectionState = .authenticated
            }
            
        default:
            break
        }
    }
}
