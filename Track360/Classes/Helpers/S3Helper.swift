//
//  S3Helper.swift
//  Track360
//
//  Created by Dinesh Vijaykumar on 12/03/2018.
//  Copyright © 2018 Conjure. All rights reserved.
//

import Foundation
import AWSS3
import AWSCore

enum S3Error: Error {
    case UploadError(String)
}

class S3Helper {
    
    static let shared = S3Helper()
    
    let accessKey = "AKIAIW7AS4WNKFF5DBJQ"
    let secretKey = "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"
    let S3BucketName = "conjure-ota"
    let transferManager: AWSS3TransferManager
    
    private init() {
        let credsProvider = AWSStaticCredentialsProvider(accessKey: accessKey, secretKey: secretKey)
        let config = AWSServiceConfiguration(region: .USEast1, credentialsProvider: credsProvider)
        
        AWSServiceManager.default().defaultServiceConfiguration = config
        transferManager = AWSS3TransferManager.default()
    }
    
    func listBucket() {
        let listRequest = AWSS3ListObjectsRequest()
        listRequest?.bucket = S3BucketName
        listRequest?.prefix = "porsche"
        AWSS3.default().listObjects(listRequest!).continueWith { (task) -> Any? in
            if let error = task.error {
                print("Listing objects failed: \(error)")
            }
            
            if let output = task.result, let contents = output.contents {
                for object in contents {
                    print(object.key!)
                }
            }
            
            return nil
        }
    }
    
    func uploadRecording(fileName: URL, completion: @escaping (URL?, Error?) -> ()) {
        let remoteName = "porsche/\(fileName.lastPathComponent)"
        let request = prepareRequest(fileName: fileName, remoteName: remoteName, contentType: "video/mp4")
        
        request.uploadProgress = {(bytesSent: Int64, totalBytesSent: Int64, totalBytesExpectedToSend: Int64) -> Void in
            DispatchQueue.main.async(execute: {() -> Void in
                
                let totalBytesSentDouble = Double(totalBytesSent)
                let total = totalBytesSentDouble + Double(totalBytesExpectedToSend)
                let progress = (totalBytesSentDouble / total) * 100
                print("Upload Progress: \(progress) %")
            })
        }
        
        upload(request: request, completion: completion)
    }
    
    func uploadImage(fileName: URL, completion: @escaping (URL?, Error?) -> ()){
        let remoteName = "porsche/\(fileName.lastPathComponent)"
        let request = prepareRequest(fileName: fileName, remoteName: remoteName, contentType: "image/jpeg")
        
        request.uploadProgress = {(bytesSent: Int64, totalBytesSent: Int64, totalBytesExpectedToSend: Int64) -> Void in
            DispatchQueue.main.async(execute: {() -> Void in
                
                let totalBytesSentDouble = Double(totalBytesSent)
                let total = totalBytesSentDouble + Double(totalBytesExpectedToSend)
                let progress = (totalBytesSentDouble / total) * 100
                print("Image Upload Progress: \(progress) %")
            })
        }
        
        upload(request: request, completion: completion)
    }
    
    func uploadCSV(videoName:String, fileName: URL, completion: @escaping (URL?, Error?) -> ()) {
        let nameIndex = videoName.index(of: ".")!
        let csvName = videoName[...nameIndex] + "csv"
        
        let remoteName = "porsche/\(csvName)"
        let request = prepareRequest(fileName: fileName, remoteName: remoteName, contentType: "text/csv")
        
        upload(request: request, completion: completion)
    }
    
    fileprivate func prepareRequest(fileName: URL,remoteName:String, contentType: String) -> AWSS3TransferManagerUploadRequest {
        let request = AWSS3TransferManagerUploadRequest()!
        request.body = fileName
        request.key = remoteName
        request.bucket = S3BucketName
        request.contentType = contentType
        
        return request
    }
    
    fileprivate func upload(request: AWSS3TransferManagerUploadRequest, completion: @escaping (URL?, Error?) -> ()) {
        transferManager.upload(request).continueWith(executor: AWSExecutor.mainThread(), block: { (task:AWSTask<AnyObject>) -> Any? in
            if let error = task.error as NSError? {
                if error.domain == AWSS3TransferManagerErrorDomain, let code = AWSS3TransferManagerErrorType(rawValue: error.code) {
                    switch code {
                    case .cancelled, .paused:
                        break
                    default:
                        completion(nil, S3Error.UploadError(error.localizedDescription))
                    }
                } else {
                    completion(nil, S3Error.UploadError(error.localizedDescription))
                }
                return nil
            }
            
            if task.result != nil {
                let url = AWSS3.default().configuration.endpoint.url
                if let publicURL = url?.appendingPathComponent(request.bucket!).appendingPathComponent(request.key!) {
                    print("Uploaded to \(publicURL) - \(Date())")
                    completion(publicURL, nil)
                }
            }
            return nil
        })
    }
    
}
