//
//  Constants.swift
//  BBTours
//
//  Created by Conjure on 07/03/2018.
//  Copyright © 2018 Conjure. All rights reserved.
//

import UIKit

//0 - default, 1 - 'SelectCamera', 2 - 'Home', 3 - 'Recording', 4 - 'Gallery'
struct Debug {
    static let ScreenSkip       = 0
}

struct Color {
    static let WhiteGray        = UIColor(red: 245/255, green: 245/255, blue: 245/255, alpha: 1.0)
}
