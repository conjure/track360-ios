# Track360 #

iOS application recording a 360-degree video in the car alongside a recoding of track, race and car telemetry data.

## Installation
* Run pod install to install all dependencies.
* Ensure latest high mobility frameworks are added in project (https://github.com/highmobility/hm-ios-sdk/releases)

## Targets
* Track360 - Build iOS application
* Track360App - Builds frameworks to integrate with Unity

## Notes
* Application requires a 360 FLY video camera to be connected
* Application currently works only with Porsche Web Simulator and has hard coded values for connection.
