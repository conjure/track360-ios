//
//  SwiftClass.swift
//  Track360App
//
//  Created by Conjure on 23/03/2018.
//  Copyright © 2018 Conjure. All rights reserved.
//

import UIKit
import Fly360SDK
import AutoAPI

public class SwiftClass: NSObject {
    
    public func sayHello() {
        print("Hello from Track360App/SwiftClass")
        
        let sdk = FLY360SDK.sharedInstance()
        print("Hello from Track360App/Fly360SDK: \(sdk)")
        
        let helper = FLY360Helper.sharedInstance()
        print("ST: FLY360Helper: \(helper)")
    }
}
